Authors: Adrian Blazquez Leon / Jaafar Belouaret


Project objectives: design the navigation flow in the CRIS Web application, and implement the interactions using the pattern MVC and servlet controllers, views with HTML and JSP. Manage the necessary information to keep the user authenticated and control the session’s navigation flow.

- [x] View Researcher List

![View ResearchersList](https://gitlab.com/fullstackapps/lab2_cris/-/raw/master/Screenshot%20from%202020-11-21%2010-11-28.png)

- [x] View Researcher MVC

![View Researchers](https://gitlab.com/fullstackapps/lab2_cris/-/raw/master/Screenshot%20from%202020-11-22%2021-20-14.png)

![ReadAllPublicationss](https://gitlab.com/fullstackapps/lab2_cris/-/raw/master/Screenshot%20from%202020-11-22%2021-21-58.png)

- [x] Login MVC: LoginView.jsp and LoginServlet

- [x] Admin MVC: AdminServlet and AdminView.jsp

- [x] CreateResearcherServlet MVC
 