<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@ include file = "Header.jsp"%>
<title>Researchers List</title>

</head>

<body>

<table>
<tr>
<th>Id</th><th>Name</th><th>Last name</th><th>URL</th><th>Email</th>
</tr>
        <tr>
            	<td> ${ri.id} </td>
                <td>${ri.name}</td>
                <td>${ri.lastName}</td>
				<td> <a href="${ri.scopusURL}"> ${ri.scopusURL} </a></td>
                <td>${ri.email}</td>
        </tr>
</table>

<h2>Publications</h2>

<table>
<c:forEach items="${publications}" var="pi">
        <tr>
			<td> <a href="PublicationServlet?id=${pi.id}"> ${pi.id}</a></td>
        </tr> 
</c:forEach>
</table>

</body>
</html>
